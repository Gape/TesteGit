# Passo a passo para criar um repositório

### Identity
*git config -- global user.email __email__*

*git config -- global user.name __nome__*

1. *git init*         \_Para iniciar um repositório local\_
2. *git add __arquivo__*  \_Colocar o arquino na área de staging\_
3. *git commit -m "__mensagem__"*   \_Manda o que estiver no staging para o repositório local\_
4. *git branch -M main*     \_Opcional / muda o nome do branch principal para main\_
5. __Crie um repositório no GitHub ou Codeberg e copie o link__
6. *git remote add origin __link__* \_Faz a ponte entre o repositório local e o remoto / origin é o apelido do repositório remoto\_
7. *git push -u origin master/main*  \_Envia o commit do repositório local para o remoto / no branch master ou main / -u é para rastreamento\_
8. *git checkout -b __novoRamo__*   \_Cria um novo branch e muda para ele\_
9. *git checkout main*  \_Volta para o ramo principal\_
10. *git merge __novoRamo__*     \_Junta o ramo principal com o __novoRamo__\_
11. *git branch -d __ramo__*    \_Remove o ramo\_
12. *git log --graph* \_Visualizar as branches de forma gráfica\_
13. *git diff* \_Ver diferenças entre arquivos modificados e no staging\_
